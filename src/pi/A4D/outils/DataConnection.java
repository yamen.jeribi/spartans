/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.outils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dagdoug
 */
public class DataConnection {
    private String url;
    private String login;
    private String password;
    private static DataConnection instance;
    private Connection connection;
    private DataConnection() {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(new File("configuration.properties")));
            url=properties.getProperty("url");
            login=properties.getProperty("login");
            password=properties.getProperty("password");
            connection = DriverManager.getConnection(url, url, password);
            System.out.println("Connexion établie!");
        } catch (IOException | SQLException ex) {
            Logger.getLogger(DataConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConnection() {
        return connection;
    }
    
    
    public static DataConnection getInstance() {
        if (instance==null) {
            instance=new DataConnection();   
        }
        return instance;
    }
}
