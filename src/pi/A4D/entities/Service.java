/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Dagdoug
 */
public class Service extends Offre {
    private int id;
    private String type;
    private String description;
    private Coupon coupons;

    public Service() {
    }
   public Service(String type, String description) {
       
        this.type = type;
        this.description = description;
    }

    public Service(int id , String titreOffre, String type, String description) {
        super(titreOffre);
        this.id = id ;
        this.type = type;
        this.description = description;
    }

    public int getId()
    {
        return id ;
    }
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Coupon getCoupons() {
        return coupons;
    }

    public void setCoupons(Coupon coupons) {
        this.coupons = coupons;
    }

}
