/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.entities;

import java.util.Objects;

/**
 *
 * @author Dagdoug
 */
public class Admin {

    private int id;
    private String login;
    private String motDePasse;
    private String email;
    private Reclamation[] reclamations;
    private Produit[] produits;
    // constructeur par défaut

    public Admin() {
    }

    public Admin(String login, String motDePasse, String email, Reclamation[] reclamations, Produit[] produits) {
        this.login = login;
        this.motDePasse = motDePasse;
        this.email = email;
        this.reclamations = reclamations;
        this.produits = produits;
    }



    //hashCode
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    //getter&setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Reclamation[] getReclamations() {
        return reclamations;
    }

    public void setReclamations(Reclamation[] reclamations) {
        this.reclamations = reclamations;
    }

    public Produit[] getProduits() {
        return produits;
    }

    public void setProduits(Produit[] produits) {
        this.produits = produits;
    }

}
