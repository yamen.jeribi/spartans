/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.entities;

import java.util.Date;

/**
 *
 * @author Dagdoug
 */
public class Produit extends Offre {

    private float prix;
    private String couleur;
    private int nombrePoints;
    private float tauxReduction;
    private Date dateLancement;
    private String etat;
    private String texte;
    private Byte photo;
    private String gouvernorat;
    private String delegation;
    private Admin admin;
    private Panier[] panier;

    public Produit() {
    }
     public Produit(float prix, String couleur, int nombrePoints, float tauxReduction, Date dateLancement, String etat, String texte, Byte photo, String gouvernorat, String delegation) {
        this.prix = prix;
        this.couleur = couleur;
        this.nombrePoints = nombrePoints;
        this.tauxReduction = tauxReduction;
        this.dateLancement = dateLancement;
        this.etat = etat;
        this.texte = texte;
        this.photo = photo;
        this.gouvernorat = gouvernorat;
        this.delegation = delegation;
    }
    public Produit(String titreOffre, float prix, String couleur, int nombrePoints, float tauxReduction, Date dateLancement, String etat, String texte, Byte photo, String gouvernorat, String delegation) {
        super(titreOffre);
        this.prix = prix;
        this.couleur = couleur;
        this.nombrePoints = nombrePoints;
        this.tauxReduction = tauxReduction;
        this.dateLancement = dateLancement;
        this.etat = etat;
        this.texte = texte;
        this.photo = photo;
        this.gouvernorat = gouvernorat;
        this.delegation = delegation;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public int getNombrePoints() {
        return nombrePoints;
    }

    public void setNombrePoints(int nombrePoints) {
        this.nombrePoints = nombrePoints;
    }

    public float getTauxReduction() {
        return tauxReduction;
    }

    public void setTauxReduction(float tauxReduction) {
        this.tauxReduction = tauxReduction;
    }

    public Date getDateLancement() {
        return dateLancement;
    }

    public void setDateLancement(Date dateLancement) {
        this.dateLancement = dateLancement;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public Byte getPhoto() {
        return photo;
    }

    public void setPhoto(Byte photo) {
        this.photo = photo;
    }

    public String getGouvernorat() {
        return gouvernorat;
    }

    public void setGouvernorat(String gouvernorat) {
        this.gouvernorat = gouvernorat;
    }

    public String getDelegation() {
        return delegation;
    }

    public void setDelegation(String delegation) {
        this.delegation = delegation;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Panier[] getPanier() {
        return panier;
    }

    public void setPanier(Panier[] panier) {
        this.panier = panier;
    }
}
